package numx_client

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	grpcformat "gitlab.com/tttachikoma/nmux/nmux_grpc"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Person struct {
	Name        string `json:"id"`
	Institution string `json:"institution"`
	Age         int    `json:"age"`
}

type Message struct {
	Message string `json:"message"`
}

func PrintResponse(resp *http.Response) {
	log.Printf("Response Status: %s", resp.Status)
	log.Printf("Response Header: %s", resp.Header)

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("Response Body Error: %s", err.Error())
	}

	message := &Message{}
	err = json.Unmarshal(respBody, message)
	if err != nil {
		log.Fatalf("Unmarshal Response Body Error: %s", err.Error())
	}
	log.Printf("Response Body Message: %s", message.Message)
}

func SendRequest(method string, url string, body io.Reader) *http.Response {
	client := http.DefaultClient

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		log.Fatalf("Create Request Error: %s", err.Error())
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Response Error: %s", err.Error())
	}

	return resp
}

func Example(addr string, name string, institution string, age int) {
	// grpc request
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := grpcformat.NewFormatClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.FormatName(ctx, &grpcformat.NameRequest{Name: name})
	if err != nil {
		log.Fatalf("could not format: %v", err)
	}
	log.Printf("Format: %s", r.GetMessage())

	r, err = c.FormatInstitution(ctx, &grpcformat.InstitutionRequest{Institution: institution})
	if err != nil {
		log.Fatalf("could not format: %v", err)
	}
	log.Printf("Format: %s", r.GetMessage())

	// http get request
	resp := SendRequest("GET", "http://"+addr+"/ping", nil)
	defer resp.Body.Close()
	PrintResponse(resp)

	//http post request
	body, err := json.Marshal(Person{Name: name, Institution: institution, Age: age})
	if err != nil {
		log.Fatalf("Marshal Request Body Error: %s", err.Error())
	}
	resp = SendRequest("POST", "http://"+addr+"/age", bytes.NewReader(body))
	defer resp.Body.Close()
	PrintResponse(resp)
}
