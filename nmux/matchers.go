package nmux

import (
	"bufio"
	"io"
	"net/http"

	"golang.org/x/net/http2"
	"golang.org/x/net/http2/hpack"
)

// Any is a Matcher that matches any connection.
func Any() Matcher {
	return func(w io.Writer, r io.Reader) bool {
		return true
	}
}

// NewHTTP1Macther returns a matcher matching the header fields of
// an HTTP 1 connection.
func NewHTTP1Macther(name, value string) Matcher {
	return func(w io.Writer, r io.Reader) bool {
		return matchHTTP1Field(w, r, name, func(gotValue string) bool {
			if gotValue == value {
				return true
			} else {
				return false
			}
		})
	}
}

// NewGRPCMatcher matches the header field and writes the
// settings to the server.
func NewGRPCMatcher(name, value string) Matcher {
	return func(w io.Writer, r io.Reader) bool {
		return matchHTTP2Field(w, r, name, func(gotValue string) bool {
			if gotValue == value {
				return true
			} else {
				return false
			}
		})
	}
}

func hasHTTP2Preface(r io.Reader) bool {
	var b [len(http2.ClientPreface)]byte
	last := 0

	for {
		n, err := r.Read(b[last:])
		if err != nil {
			return false
		}

		last += n
		eq := string(b[:last]) == http2.ClientPreface[:last]
		if last == len(http2.ClientPreface) {
			return eq
		}
		if !eq {
			return false
		}
	}
}

func matchHTTP1Field(w io.Writer, r io.Reader, name string, matches func(string) bool) (matched bool) {
	req, err := http.ReadRequest(bufio.NewReader(r))
	if err != nil {
		return false
	}
	return matches(req.Header.Get(name))
}

func matchHTTP2Field(w io.Writer, r io.Reader, name string, matches func(string) bool) (matched bool) {
	if !hasHTTP2Preface(r) {
		return false
	}
	done := false
	framer := http2.NewFramer(w, r)
	hdec := hpack.NewDecoder(uint32(4<<10), func(hf hpack.HeaderField) {
		if hf.Name == name {
			done = true
			if matches(hf.Value) {
				matched = true
			}
		}
	})
	for {
		f, err := framer.ReadFrame()
		if err != nil {
			return false
		}

		switch f := f.(type) {
		case *http2.SettingsFrame:
			// Sender acknoweldged the SETTINGS frame. No need to write
			// SETTINGS again.
			if f.IsAck() {
				break
			}
			if err := framer.WriteSettings(); err != nil {
				return false
			}
		case *http2.ContinuationFrame:
			if _, err := hdec.Write(f.HeaderBlockFragment()); err != nil {
				return false
			}
			done = done || f.FrameHeader.Flags&http2.FlagHeadersEndHeaders != 0
		case *http2.HeadersFrame:
			if _, err := hdec.Write(f.HeaderBlockFragment()); err != nil {
				return false
			}
			done = done || f.FrameHeader.Flags&http2.FlagHeadersEndHeaders != 0
		}

		if done {
			return matched
		}
	}
}
