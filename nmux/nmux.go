package nmux

import (
	"errors"
	"io"
	"net"
	"sync"
)

// Matcher is a match that can also write response (say to do handshake).
type Matcher func(io.Writer, io.Reader) bool

// ErrListenerClosed is returned from muxListener.Accept when the underlying
// listener is closed.
var ErrListenerClosed = errors.New("mux: listener closed")

// ErrServerClosed is returned from muxListener.Accept when mux server is closed.
var ErrServerClosed = errors.New("mux: server closed")

// New instantiates a new connection multiplexer.
func New(l net.Listener) NMux {
	return &nMux{
		root:   l,
		bufLen: 1024,
		donec:  make(chan struct{}),
	}
}

type NMux interface {
	// RegisterMatcher returns a net.Listener that accepts only the
	// connections that matched by at least of the matcher writers.
	//
	// The order used to call Match determines the priority of matchers.
	RegisterMatcher(Matcher) net.Listener
	// Serve starts multiplexing the listener. Serve blocks and perhaps
	// should be invoked concurrently within a go routine.
	Serve() error
	// Closes nmux server and stops accepting any connections on listener
	Close()
}

type nMux struct {
	root   net.Listener // rootListener
	bufLen int
	// rootListener负责接收请求，matcherListener负责匹配和传递请求
	mls   []matcherListener
	donec chan struct{} // 用于平滑退出
}

type matcherListener struct {
	matcher Matcher
	// matcher负责匹配请求，muxListener负责接收匹配成功的请求
	muxl muxListener
}

// 创建一个muxListener，和传入的matcher一起实例化一个matcherListener
// 并添加到nmux的[]matcherListener中
func (m *nMux) RegisterMatcher(matcher Matcher) net.Listener {
	muxl := muxListener{
		Listener: m.root,
		connc:    make(chan net.Conn, m.bufLen),
		donec:    make(chan struct{}),
	}
	m.mls = append(m.mls, matcherListener{matcher: matcher, muxl: muxl})
	return muxl
}

// 1. rootListener接收请求，并创建goroutine
//    交给各个matcherListener处理请求
// 2. rootListener循环接收下一个请求
func (m *nMux) Serve() error {
	var wg sync.WaitGroup

	defer func() {
		m.Close()
		wg.Wait()
		m.closeConncChans()
	}()

	for {
		c, err := m.root.Accept()
		if err != nil {
			return err
		}

		wg.Add(1)
		go m.serve(c, m.donec, &wg)
	}
}

// 3. matcher按注册顺序匹配net.Conn
// 4. 匹配成功则将net.Conn传递给muxListener中的connc
func (m *nMux) serve(c net.Conn, donec <-chan struct{}, wg *sync.WaitGroup) {
	defer wg.Done()

	muc := newMuxConn(c)
	for _, ml := range m.mls {
		matched := ml.matcher(muc, muc.startSniffing())
		if matched {
			muc.doneSniffing()
			select {
			case ml.muxl.connc <- muc:
			case <-donec:
				c.Close()
			}
			return
		}
	}
	c.Close()
}

type muxListener struct {
	net.Listener
	// 用于接收匹配成功后被传递过来的net.Conn
	connc chan net.Conn
	donec chan struct{} // 用于平滑退出
}

// 由于每个muxListener都被某个goroutine中的服务程序监控，
// 即go serveXX(muxListener)，且muxListener组合了net.Listener，
// 因此可以通过重写Accept()方法将匹配成功的连接传递给服务程序。
// 同时，服务程序和匹配程序在不同的goroutine中，
// 因此，需要使用chan来传递net.Conn
func (muxl muxListener) Accept() (net.Conn, error) {
	select {
	case c, ok := <-muxl.connc:
		if !ok {
			return nil, ErrListenerClosed
		}
		return c, nil
	case <-muxl.donec:
		return nil, ErrServerClosed
	}
}

// 1. 关闭rootListener，m.Serve()将返回
//    "use of closed network connection"
// 2. 通过donec给muxServer发送ErrServerClosed
//    让各个muxServer调用Shutdown()，平滑退出
// 3. 通过m.Serve()的defer机制关闭所有muxListener
func (m *nMux) Close() {
	m.root.Close()
	m.closeDoneChans()
}

func (m *nMux) closeDoneChans() {
	select {
	case <-m.donec:
		// Already closed. Don't close again
	default:
		close(m.donec)
	}
	for _, ml := range m.mls {
		select {
		case <-ml.muxl.donec:
			// Already closed. Don't close again
		default:
			close(ml.muxl.donec)
		}
	}
}

func (m *nMux) closeConncChans() {
	for _, ml := range m.mls {
		ml.muxl.Listener.Close()
	}
}

// MuxConn wraps a net.Conn and provides transparent sniffing of connection data.
type MuxConn struct {
	net.Conn
	buf bufferedReader
}

func newMuxConn(c net.Conn) *MuxConn {
	return &MuxConn{
		Conn: c,
		buf:  bufferedReader{source: c},
	}
}

func (m *MuxConn) Read(p []byte) (int, error) {
	return m.buf.Read(p)
}

func (m *MuxConn) startSniffing() io.Reader {
	m.buf.reset(true)
	return &m.buf
}

func (m *MuxConn) doneSniffing() {
	m.buf.reset(false)
}
