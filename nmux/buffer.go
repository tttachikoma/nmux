package nmux

import (
	"bytes"
	"io"
)

// 对于同一个conn
// 1. 在第一次调用Read(p []byte)时，需要从socket缓冲区中读取数据，并放在buffer中
// 2. 在后续的调用中，需要直接从buffer中读取数据
// 3. 每次匹配conn时，需要先将sniffing置为True，bufferRead置为0，再读取数据
// 3.1 若bufferSize=bufferRead=0，说明是第一次匹配，执行1
// 3.2 若bufferSize>bufferRead，说明不是第一次匹配，将直接从buffer中读取数据
// 4. 在匹配结束后，需要先将sniffing置为False，bufferRead置为0，再进入处理阶段
// 4.1 若bufferSize>bufferRead，说明buffer中存在数据未被处理
// 4.2 若bufferSize=bufferRead，说明buffer中所有数据均被处理，应该继续从socket缓冲区中读取数据
type bufferedReader struct {
	source     io.Reader
	buffer     bytes.Buffer
	bufferRead int // buffer已被读取的字节数
	bufferSize int // buffer中存储的字节数
	sniffing   bool
	lastErr    error
}

// 1. 每次匹配conn时，需要先将sniffing置为True，bufferRead置为0，再读取数据
// 1.1 若bufferSize=bufferRead=0，说明是第一次匹配，执行1
// 1.2 若bufferSize>bufferRead，说明不是第一次匹配，将直接从buffer中读取数据
// 2. 在匹配结束后，需要先将sniffing置为False，bufferRead置为0，再进入处理阶段
// 2.1 若bufferSize>bufferRead，说明buffer中存在数据未被处理
// 2.2 若bufferSize=bufferRead，说明buffer中所有数据均被处理，应该继续从socket缓冲区中读取数据
func (s *bufferedReader) Read(p []byte) (int, error) {
	if s.bufferSize > s.bufferRead {
		bn := copy(p, s.buffer.Bytes()[s.bufferRead:s.bufferSize])
		s.bufferRead += bn
		return bn, s.lastErr
	} else if !s.sniffing && s.buffer.Cap() != 0 {
		s.buffer = bytes.Buffer{}
	}

	sn, sErr := s.source.Read(p)
	if sn > 0 && s.sniffing {
		s.lastErr = sErr
		if wn, wErr := s.buffer.Write(p[:sn]); wErr != nil {
			return wn, wErr
		}
	}
	return sn, sErr
}

func (s *bufferedReader) reset(snif bool) {
	s.sniffing = snif
	s.bufferRead = 0
	s.bufferSize = s.buffer.Len()
}
