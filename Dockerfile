FROM golang:1.18

ENV path /go/src/nmux
COPY . ${path}
WORKDIR ${path}

RUN go env -w GO111MODULE=on
RUN go env -w GOPROXY=https://goproxy.cn,direct

RUN go build -v -o nmux_bin

EXPOSE 9091

ENTRYPOINT [ "./nmux_bin" ]


