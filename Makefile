tag=$(shell git show -s --format=%ct $(shell git rev-parse origin/master))

show:
	echo ${tag}

upgrade:
ifeq (${tag}, )
	@echo a tag is required
else
	helm upgrade -i --set image.tag=${tag} nmux ./nmux-dev/
endif

.PHONY: show upgrade