[toc]

### 项目简介

- `nmux`是一个用`golang`实现的服务端框架。它能够在启动后只监听一个端口的情况下同时接受`HTTP1`请求和`gRPC`请求

- `nmux`根据请求头（Header）中的键值对`Content-Type: application/json`或`Content-Type: application/grpc`决定分发给`HTTP1 Server`或`gRPC Server`

- `nmux`实现了平滑退出功能

  - 平滑退出（Graceful Shutdown）：服务端不再接收新的请求，且等到现有请求都处理完成才退出，如请求处理时间过长，则在等待⼀个较长时间（如10秒）后强制退出
  - 当接收到`syscall.SIGINT`或`syscall.SIGTERM`信号、收到`HTTP1`的`/stop`请求、收到`gRPC`的`stop`调用时，服务器能实现平滑退出

- `nmux`实现了日志记录功能，日志文件存放在当前目录下的`log`目录中

- `nmux`基本架构主要参考[`cmux`](https://github.com/soheilhy/cmux)，项目结构如下：

  ```
  .
  ├── Dockerfile
  ├── .gitlab-ci.yml
  ├── go.mod
  ├── go.sum
  ├── main.go			// 调用nmux_server包，启动一个nmux服务
  ├── nmux 			// 实现了在只监听一个端口的情况下将HTTP1和gRPC请求分配给相应的处理程序的功能
  │   ├── buffer.go
  │   ├── matchers.go
  │   └── nmux.go
  ├── nmux_client		// 涵盖多个HTTP1请求和多个gRPC请求的访问示例
  │   └── example.go
  ├── nmux_grpc		// 根据proto文件自动生成，实现了gRPC服务端和客户端
  │   ├── nmux_grpc.pb.go
  │   ├── nmux.pb.go
  │   └── nmux.proto
  └── nmux_server		// 实现了HTTP1和gRPC服务，涵盖多种HTTP1和gRPC请求的处理程序
      └── example.go
  
  4 directories, 13 files
  ```

- `nmux`在`Linux`上的简单使用

  - 进入`HOME`目录，新建一个项目目录并进入

    ```bash
    $ cd
    $ mkdir example
    $ cd example
    ```

  - 为项目启动依赖项跟踪

    ```bash
    $ go mod init example
    go: creating new go.mod: module example
    ```

  - 将`nmux`包导入`go.mod`文件中

    ```bash
    $ go get -u gitlab.com/tttachikoma/nmux@latest
    ```

  - 新建`main.go`文件

    ```bash
    $ touch main.go
    ```

  - 将以下内容添加到`main.go`文件中

    ```go
    package main
    
    import (
    	"gitlab.com/tttachikoma/nmux/nmux_server"
    )
    
    func main() {
    	nmux_server.Example("0.0.0.0:9091")
    }
    
    ```

  - 监听`0.0.0.0:9091`，并启动`nmux`服务

    ```bash
    $ go run main.go
    2022/04/19 00:21:31 example.go:211: Listen on 0.0.0.0:9091
    2022/04/19 00:21:31 example.go:237: Start NMUX Server
    2022/04/19 00:21:31 example.go:200: Start ServeGRPC
    2022/04/19 00:21:31 example.go:159: Start ServeHTTP
    2022/04/19 00:21:31 example.go:75: Start ServeError
    ```

  - 在另一个命令行窗口向`127.0.0.1:9091`发送`/ping`请求，可获得响应

    ```bash
    $ curl -H "Content-Type:application/json" 127.0.0.1:9091/ping
    {"message":"Welcome to netease!"}
    ```

  - 在另一个命令行窗口向`127.0.0.1:9091`发送`/stop`请求，`nmux`服务将平滑退出

    ```bash
    $ curl -H "Content-Type:application/json" 127.0.0.1:9091/stop
    {"message":"The server is shutting down"}
    ```

- `nmux`的开发和部署流程

  - 本项目已注册`Runner`，当一个新的提交发生，`Runner`将自动执行`.gitlab-ci.yml`文件，启动`CI/CD`流程。

  - 本项目的`CI/CD`流程包括以下两个阶段，以下是每个阶段执行的工作：

    - `test`
      - 对项目执行`go vet`，`staticcheck`，`go test`检查
    - `docker`
      - 使用`docker build`根据`Dockerfile`构建`nmux`镜像
      - 登录腾讯云镜像仓库，将镜像推送到仓库中

  - 本项目支持在`kubernetes`环境下部署

    - `make upgrade`

      - `upgrade`目标执行了`helm upgrade`命令，该命令有以下功能

      - 根据`nmux-dev`目录下的`kubernetes`配置文件在`nmux`命名空间下部署（或更新）命名为`nmux-dev`的`deployment`和`service`

        - 若当前环境下没有`nmux`命名空间，则使用以下命令创建

          - ```bash
            $ kubectl create namespace nmux
            ```

      - 从腾讯云镜像仓库中拉取`master`最新提交构建的`nmux`镜像

      - 使用以下命令可以发现`deployment`和`service`均已被成功创建

        - ```bash
          $ kubectl get deployments -n nmux
          $ kubectl get svc -n nmux
          ```

      - 使用以下命令可以查看`deployment`管理的两个`pod`

        - ```bash
          $ kubectl get pods -n nmux
          ```

      - 使用以下命令向`service`的`cluster-ip`发送`/ping`请求，可获得响应

        - ```bash
          $ curl -H "Content-Type:application/json" CLUSTER-IP:9091/ping
          {"message":"Welcome to netease!"}
          ```

          

### 实现原理

- **`nmux`框架流程图**![nmux](./image/nmux.png)

- **创建`rootListener`**

  - 创建`rootListener`监听地址和端口`addr`

    - 参考资料：[后端监听ip地址的三种主要的方式](https://segmentfault.com/a/1190000018629247)
    - 后端监听IP地址的三种主要的方式
      - **监听到127.0.0.1**：本机通过127.0.0.1访问成功，局域网主机通过局域网IP访问失败，外网主机通过外网IP访问失败
      - **监听到主机内网IP**：本机通过127.0.0.1访问失败，局域网主机通过局域网IP访问成功，外网主机通过外网IP访问成功
      - **监听到0.0.0.0**：
    - 在实际应用中，最好的监听IP地址方式为：**监听到0.0.0.0**

  - 初始化`nmux`，用于将请求分配给不同的服务端

    - `func New(l net.Listener) NMux` 用于初始化一个`nMux`结构体，该结构体实现了`NMUX`接口

    - `nMux`结构体定义如下：

    - ```go
      type nMux struct {
      	root   net.Listener // rootListener
      	bufLen int
          // rootListener负责接收请求，matcherListener负责匹配和传递请求
      	mls    []matcherListener
      	donec  chan struct{} // 用于平滑退出
      }
      ```

    - `NMUX`接口定义如下：

    - ```go
      type NMux interface {
      	// RegisterMatcher returns a net.Listener that accepts only the
      	// connections that matched by at least of the matcher writers.
      	//
      	// The order used to call Match determines the priority of matchers.
      	RegisterMatcher(Matcher) net.Listener
      	// Serve starts multiplexing the listener. Serve blocks and perhaps
      	// should be invoked concurrently within a go routine.
      	Serve() error
      	// Closes nmux server and stops accepting any connections on listener
      	Close()
      }
      ```

- **创建`matcher`和`muxListener`**

  - `matcher`定义如下：

  - ```go
    // Matcher is a match that can also write response (say to do handshake).
    type Matcher func(io.Writer, io.Reader) bool
    ```

  - 创建`http1Matcher`，用于匹配`HTTP1`请求

    - ```go
      // NewHTTP1Macther returns a matcher matching the header fields of
      // an HTTP1 connection.
      func NewHTTP1Macther(name, value string) Matcher
      
      httpMatcher := nmux.NewHTTP1Macther("Content-Type", "application/json")
      ```

    - 如何判断某个请求为`HTTP1`请求

      - ```go
        // 使用ReadRequest对连接进行解析
        // 使用Header.Get(key)查看Request Header中指定Field的值
        req, _ := http.ReadRequest(bufio.NewReader(net.Conn))
        value := req.Header.Get(key) 
        ```

        

  - 创建`grpcMatcher`，用于匹配`gRPC`请求

    - ```go
      // NewGRPCMatcher matches the header field and writes the
      // settings to the server.
      func NewGRPCMatcher(name, value string) Matcher
      
      grpcMacther := nmux.NewGRPCMatcher("content-type", "application/grpc")
      ```

    - 如何判断某个请求为`gRPC`请求

  - 创建`errorMatcher`，用于匹配非`HTTP1`和非`gRPC`请求，代表匹配失败

    - ```go
      // Any is a Matcher that matches any connection.
      func Any() Matcher {
      	return func(w io.Writer, r io.Reader) bool {
      		return true
      	}
      }
      
      errorMatcher := nmux.Any()
      ```

  - 将所有`matcher`注册到`nmux`的`[]matcherListener`中

    - `matcherListener`结构体定义如下：

    - ```go
      type matcherListener struct {
      	matcher Matcher
          // matcher负责匹配请求，muxListener负责接收匹配成功的请求
      	muxl    muxListener
      }
      ```

    - ```go
      // 创建一个muxListener，和传入的matcher一起实例化一个matcherListener
      // 并添加到nmux的[]matcherListener中
      func (m *nMux) RegisterMatcher(matcher Matcher) net.Listener
      
      // We first match the connection against HTTP2 fields. If matched, the
      // connection will be sent through the "grpcListener" listener.
      grpcListener := m.RegisterMatcher(grpcMacther)
      // Otherwise, we match it againts HTTP1 fields. If matched,
      // it is sent through the "http1Listener" listener.
      http1Listener := m.RegisterMatcher(http1Macther)
      // If not matched by above matchers, we assume it is an error connection.
      errorListener := m.RegisterMatcher(errorMatcher)
      ```

- **创建`muxServer`**

  - 创建`http1Server`，用于处理`http1Listener`接收的请求

    - ```go
      func serveHTTP(l net.Listener) {
      	http1Server := &http.Server{
      		Handler: router,
      	}
      	http1Server.Serve(l)
      }
      ```

  - 创建`grpcServer`，用于处理`grpcListener`接收的请求

    - ```go
      func serveGRPC(l net.Listener) {
      	grpcs := grpc.NewServer()
          grpcformat.RegisterFormatServer(grpcs, &grpcServer{})
      	grpcs.Serve(l)
      }
      ```

  - 创建`errorServer`，用于处理`errorListener`接收的请求

    - ```go
      func serveError(l net.Listener) {
      	errorServer := &http.Server{
      		Handler: &ErrorHandler{},
      	}
      	errorServer.Serve(l)
      }
      ```

  - 为所有`Server`分别创建`goroutine`

    - ```go
      go serveGRPC(grpcListener)
      go serveHTTP(http1Listener)
      go serveError(errorListener)
      ```

- **`rootListener`接收请求**

  - ```go
    // 1. rootListener接收请求，并创建goroutine
    //    交给各个matcherListener处理请求
    // 2. rootListener循环接收下一个请求
    // 注：以下代码省略了平滑退出的相关内容
    func (m *nMux) Serve() error {
    	for {
    		c, err := m.root.Accept()
    		if err != nil {
    			return err
    		}
    		go m.serve(c)
    	}
    }
    ```

- **`matcher`匹配请求**

  - `muxListener`结构体定义和如下：

  - ```go
    type muxListener struct {
    	net.Listener
        // 用于接收匹配成功后被传递过来的net.Conn
    	connc chan net.Conn
    	donec chan struct{} // 用于平滑退出
    }
    ```

  - ```go
    // 3. matcher按注册顺序匹配net.Conn
    // 4. 匹配成功则将net.Conn传递给同一个matcherListener
    // 中的muxListener中的connc
    // 注：以下代码省略了平滑退出的相关内容
    // 注：以下代码省略了对net.Conn的封装处理
    func (m *nMux) serve(c net.Conn) {
    	for _, ml := range m.mls {
    		matched := ml.matcher(c, c)
    		if matched {
    			ml.muxl.connc <- c
    			return
    		}
    	}
    	c.Close()
    }
    ```

- **`muxListener`接收请求**

  - ```go
    // 5. muxListener接受传递过来的net.Conn
    // 6. 监控该muxListener的服务程序将处理该连接请求
    
    // 由于每个muxListener都被某个goroutine中的服务程序监控，
    // 即go serveXX(muxListener)，且muxListener组合了net.Listener，
    // 因此可以通过重写Accept()方法将匹配成功的连接传递给服务程序。
    // 同时，服务程序和匹配程序在不同的goroutine中，
    // 因此需要使用chan来传递net.Conn
    func (muxl muxListener) Accept() (net.Conn, error) {
    	select {
    	case c, ok := <-muxl.connc:
    		if !ok {
    			return nil, ErrListenerClosed
    		}
    		return c, nil
    	case <-muxl.donec:
    		return nil, ErrServerClosed
    	}
    }
    ```



- **平滑退出**

  - 在退出主程序前，需要关闭的资源有

    - `rootListener`
    - `muxListener`
    - `muxServer`
    - 正在处理中的`net.conn`

  - 主要思路：模仿`http.Server`的`shutdown`方法的`done`机制

  - 使用`signal.Notify`监听信号

  - ```go
    var c chan os.Signal = make(chan os.Signal, 1)
    // 键入CTRL+C会向程序发送syscall.SIGINT信号，该信号将被传入c
    signal.Notify(c, []os.Signal{syscall.SIGTERM, syscall.SIGINT}...)
    ```

  - 创建`goroutine`负责接收终止信号，启动平滑退出

  - ```go
    func func Example(ip string) {
        ...
        go serveGRPC(grpcListener)
        go serveHTTP(http1Listener)
        go serveError(errorListener)
    
        // 因为m.Serve()内部是一个FOR死循环，
        // 所以要在执行Serve()前创建goroutine
        // 在未接收到终止信号前，<-c是阻塞的
        // 在接收到信号后，<-c非阻塞，执行Close()
        go func() {
            <-c
            m.Close()
        }()
    
        m.Serve()
    	// 等待十秒，让muxServer处理完所有请求
        time.Sleep(10 * time.Second)
        logger.Println("Stop NMUX Server")
    }
    ```

  - 关闭`rootListener`，`m.Serve()`将返回`"use of closed network connection"`

  - 通过`donec`给`muxServer`发送`ErrServerClosed`让各个`muxServer`调用`Shutdown()`，平滑退出

  - ```go
    func (m *nMux) Close() {
    	m.root.Close()
    	m.closeDoneChans()
    }
    
    // donec: chan struct{}
    // 关闭rootListener的donec
    // 关闭muxListener的donec
    func (m *nMux) closeDoneChans() {
    	close(m.donec)
    	for _, ml := range m.mls {
    		close(ml.muxl.donec)
    	}
    }
    
    // 利用select机制，在未关闭donec前，该case阻塞，不会执行
    // 在关闭donec后，该case非阻塞，返回ErrServerClosed
    func (muxl muxListener) Accept() (net.Conn, error) {
    	select {
    	case c, ok := <-muxl.connc:
    		return c, nil
    	case <-muxl.donec:
    		return nil, ErrServerClosed
    	}
    }
    
    // 接受到ErrServerClosed后，muxServer调用Shutdown()
    if err := http1Server.Serve(l); err == nmux.ErrServerClosed {
        http1Server.Shutdown(ctx)
    }
    ```

  - 通过`m.Serve()`的`defer`机制关闭所有`muxListener`

  - ```go
    func (m *nMux) Serve() error {
    	defer func() {
            // 存在非正常关闭的可能，因此要再调用一遍Close()
    		m.Close()
    		m.closeConncChans()
    	}()
    
    	for {
            // root调用Close()后，返回的err包含
            // "use of closed network connection"
    		c, err := m.root.Accept()
    		if err != nil {
    			return err
    		}
    		go m.serve(c)
    	}
    }
    
    // 关闭所有muxListener
    func (m *nMux) closeConncChans() {
    	for _, ml := range m.mls {
    		ml.muxl.Listener.Close()
    	}
    }
    ```





- **`net.Conn`的封装处理**

  - 当`rootListener`接收到一个`net.conn`时，需要做以下处理

    - 首先，第一个`matcher`将调用`conn`的`Read`方法从`socket`缓冲区中读取数据
    - 接着，该`matcher`将解析读取到的数据，判断该请求是`HTTP1`请求、`gRPC`请求还是其他类型的请求
    - 匹配成功后`conn`将被传递给对应的服务程序，服务程序再一次调用`conn`的`Read`方法读取数据
    - 但此时`socket`缓冲区里数据已经不是原来的数据，因此会读取失败
    - 另一方面，如果匹配失败，那`conn`会被传递给第二个`matcher`，同样该`matcher`会调用`Read`方法读取数据，但读取失败
    - 解决方案是第一次调用`Read`方法读取数据的时候，将数据存放在一个指定的缓冲区中；接下来对`Read`方法的所有调用将直接读取该缓冲区，直到缓冲区数据都被读取过
    - 因此，在`nmux`中，使用自定义的`MuxConn`结构体对`conn`作封装处理

  - `MuxConn`结构体定义如下：

    - ```go
      // MuxConn wraps a net.Conn and provides transparent sniffing of connection data.
      type MuxConn struct {
      	net.Conn
      	buf bufferedReader
      }
      
      // 对于同一个conn
      // 1. 在第一次调用Read(p []byte)时，需要从socket缓冲区中读取数据，并复制到buffer中
      // 2. 在后续的调用中，若buffer存在数据未被读取，则需要直接从buffer中读取数据
      // 3. 在后续的调用中，若buffer所有数据均被读取，则继续从socket缓冲区中读取数据
      
      // 重写Read(p []byte)以实现上述思路
      // 1. 每次匹配conn时，需要先将sniffing置为True，bufferRead置为0，再读取数据
      // 1.1 若bufferSize=bufferRead=0，说明是第一次匹配，需要从socket缓冲区中读取数据，并复制到buffer中
      // 1.2 若bufferSize>bufferRead，说明不是第一次匹配，将直接从buffer中读取数据
      // 1.3 若bufferSize=bufferRead!=0，说明buffer所有数据均被读取，则继续从socket缓冲区中读取数据，并复制到buffer中
      // 2. 在匹配结束后，需要先将sniffing置为False，bufferRead置为0，再进入处理阶段
      // 2.1 若bufferSize>bufferRead，说明buffer中存在数据未被处理，需要从buffer中读取数据
      // 2.2 若bufferSize=bufferRead，说明buffer中所有数据均被处理，应该继续从socket缓冲区中读取数据，但不再需要将数据复制到buffer中
      type bufferedReader struct {
      	source     io.Reader
      	buffer     bytes.Buffer
      	bufferRead int // buffer已被读取的字节数
      	bufferSize int // buffer中存储的字节数
      	sniffing   bool
      	lastErr    error
      }
      ```

- 使用`helm`生成`kubernetes`配置文件模板

  - 使用`helm create`创建指定名称的`chart`，目录结构如下

    - ```bash
      $ helm create nmux-dev
      
      nmux-dev/
      ├── .helmignore
      ├── charts/
      ├── Chart.yaml
      ├── templates/
      │   ├── deployment.yaml
      │   ├── _helpers.tpl
      │   ├── hpa.yaml
      │   ├── ingress.yaml
      │   ├── NOTES.txt
      │   ├── serviceaccount.yaml
      │   ├── service.yaml
      │   └── tests/
      │       └── test-connection.yaml
      └── values.yaml
      ```

  - 本项目不需要使用到`chart`中全部模板，删改后目录结构如下

    - ```bash
      nmux-dev/
      ├── .helmignore
      ├── Chart.yaml
      ├── templates/
      │   ├── deployment.yaml
      │   ├── _helpers.tpl
      │   ├── NOTES.txt
      │   └── service.yaml
      └── values.yaml
      ```

  - 主要修改`values.yaml`文件，例如：

    - ```yaml
      replicaCount: 2
      
      image:
        repository: ccr.ccs.tencentyun.com/tttachikoma/nmux
        pullPolicy: IfNotPresent
        tag: "" # tag由外部命令传入
        containerPort: 9091
        
      fullnameOverride: "nmux-dev"
      namespace: nmux
      
      service:
        type: ClusterIP
        port: 9091
      ```

  - 使用`helm upgrade`部署或更新`nmux`

    - ```bash
      $ helm upgrade -i --set image.tag=${tag} nmux ./nmux-dev/
      ```

  - 使用以下命令可以发现`deployment`和`service`均已被成功创建

    - ```bash
      $ kubectl get deployments -n nmux
      $ kubectl get svc -n nmux
      ```

  - 使用以下命令可以查看`deployment`管理的两个`pod`

    - ```bash
      $ kubectl get pods -n nmux
      ```

  - 使用以下命令向`service`的`cluster-ip`发送`/ping`请求，可获得响应

    - ```bash
      $ curl -H "Content-Type:application/json" CLUSTER-IP:9091/ping
      {"message":"Welcome to netease!"}
      ```

      



### 环境配置

- Go环境配置

  - 参考资料：https://go.dev/doc/install

  - 从https://go.dev/dl/中选择合适的版本，下载到本地

    - 如`go1.18.1.linux-amd64.tar.gz`

  - 删除之前的版本（如果它存在，且被安装到了`/usr/local/go`目录中

    - ```bash
      $ rm -rf /usr/local/go
      ```

  - 将下载的版本解压到`/usr/local`目录，形成新的`/usr/local/go`目录

    - ```bash
      $ tar -C /usr/local -xzf go1.18.1.linux-amd64.tar.gz
      ```

  - 将`/usr/local/go/bin `加入环境变量`PATH`中

    - 将以下命令添加到`$HOME/.profile`或`/etc/.profile`中

      - ```bash
        export PATH=$PATH:/usr/local/go/bin
        ```

    - 重启或执行以下命令使新的环境变量生效

      - ```bash
        $ source $HOME/.profile
        or
        $ source /etc/.profile
        ```

  - 验证是否安装成功

    - ```bash
      $ go version
      ```

  

  - 参考资料：https://goproxy.cn/

  - 添加代理

    - ```bash
      $ go env -w GO111MODULE=on
      $ go env -w GOPROXY=https://goproxy.cn,direct
      ```

      

- Docker环境配置

  - 参考资料：https://docs.docker.com/engine/install/ubuntu/

  - 在`Ubuntu 18.04`上的安装步骤：

    - ```bash
      $ sudo apt-get update
      $ sudo apt-get install \
          apt-transport-https \
          ca-certificates \
          curl \
          gnupg \
          lsb-release
      $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
      $ echo \
        "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
      $ sudo apt-get install docker-ce docker-ce-cli containerd.io
      $ apt-cache madison docker-ce
      $ sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
      $ sudo docker run hello-world #验证是否成功安装
      ```

  

  - 参考资料：https://gist.github.com/y0ngb1n/7e8f16af3242c7815e7ca2f0833d3ea6

  - Docker Hub镜像加速器

    - ```bash
      $ sudo mkdir -p /etc/docker
      $ sudo tee /etc/docker/daemon.json <<-'EOF'
      {
          "registry-mirrors": [
              "https://reg-mirror.qiniu.com",
              "https://hub-mirror.c.163.com",
              "https://mirror.ccs.tencentyun.com",
              "https://dockerhub.azk8s.cn"
          ]
      }
      EOF
      sudo systemctl daemon-reload
      sudo systemctl restart docker
      ```

  

  - 参考资料：https://docs.docker.com/engine/install/linux-postinstall/

  - 无sudo权限用户使用docker

    - ```bash
      # root
      # 将用户添加到docker组中
      $ groupadd docker
      $ usermod -aG docker $USER
      $ newgrp docker
      
      # user
      # 测试是否设置成功
      $ docker run hello-world
      ```

  

  - 参考资料：https://segmentfault.com/a/1190000038921337 (内附一图流)

  - 常用命令

    - ```bash
      # 查看全部镜像
      $ docker image ls
      # 删除所有<none>镜像
      $ docker rmi $(docker images | grep "none" | awk '{print $3}') 
       
      # 查看全部容器
      $ docker ps -a
      # 启动容器以后台方式运行，且进行端口映射，且为容器命名
      $ docker run -itd -p <host_port>:<container_port> --name <container_name> <image_name>
      # 进入运行中的容器
      $ docker exec -it <container_name> /bin/bash
      # 删除Exited状态的容器
      $ docker rm $(docker ps -qf status=exited)
      
      # 待补充
      ```

      

- Git配置

  - 参考资料

    - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
    - https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup

  - ```bash
    # 安装git
    $ sudo apt-get update
    $ sudo apt install git
    $ git --version #检查是否成功安装
    
    # 配置git
    $ git config --global user.name "your name"
    $ git config --global user.email "your email"
    $ git config --list --show-origin #查看是否配置成功
    ```

    

  - 参考资料：https://docs.gitlab.com/ee/user/ssh.html

  - 为gitlab帐户添加SSH密钥

    - 执行`ssh-keygen -t rsa -C 'key_name'`生成密钥对
    - 执行`ls ~/.ssh`发现有两个新文件生成`id_rsa`和`id_rsa.pub`
    - 将`id_rsa.pub`中的内容复制到gitlab上
      - 右上角用户头像 -> Edit profile -> SSH keys

    

- Gitlab-Runner配置

  - 为指定项目注册Runner

    - Project -> Settings -> CI/CD -> Runners -> Specific runners -> Show runner installation instructions

    

  - 参考资料：

    - https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding
    - https://gitlab.com/gitlab-org/gitlab-runner/-/issues/5026

  - Runner使用Docker命令

    - 使用`which docker`命令查看本机的Docker Daemon Socket所在目录，如`/var/run/docker.sock:/var/run/docker.sock`或`/usr/bin/docker:/usr/bin/docker`

    - 打开`~/.gitlab-runner/config.toml`

    - 为指定Runner的`[[runners]] -> [runners.docker] -> volumes = ["/cache"]`添加上述目录，如`volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`

      

- 配置防火墙

  - 参考资料：https://www.jianshu.com/p/451218449f54

  - 常用命令：

    - ```bash
      # 启动ufw
      $ ufw enable
      
      # 查看防火墙状态及规则列表
      $ ufw status
      
      # 开放SSH端口
      $ ufw allow 22
      
      # 关闭SSH端口
      $ ufw delete allow 22/tcp #指定具体的规则删除
      
      # 关闭ufw
      $ ufw disable
      ```



- 配置kubernetes

  - 参考资料：

    - https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
    - https://anuoua.github.io/2020/07/01/k8s%E5%8D%95%E6%9C%BA%E9%83%A8%E7%BD%B2/
    - https://zhuanlan.zhihu.com/p/46341911

  - 关闭交换区

    - ```bash
      $ sudo swapoff -a
      ```

  - 安装 kubeadm、kubelet 和 kubectl

    - 由于`google`源不可用，换用中科大的下载源

      - ```bash
        $ cat <<EOF > /etc/apt/sources.list.d/kubernetes.list
        > deb http://mirrors.ustc.edu.cn/kubernetes/apt kubernetes-xenial main
        > EOF
        ```

    - 安装

      - ```bash
        $ sudo apt-get update
        $ sudo apt-get install -y kubelet kubeadm kubectl
        $ sudo apt-mark hold kubelet kubeadm kubectl
        ```

  - 运行`kubeadm`

    - 由于`kubernetes`的官方镜像地址`k8s.gcr.io`·不可访问，所以换用阿里云的镜像地址`registry.aliyuncs.com/google_containers`

      - 列出`kubeadm`需要的镜像列表，这些镜像都是运行`kubeadm`所需的镜像

        - ```bash
          $ kubeadm config images list
          ```

      - 把`k8s.gcr.io`替换成`registry.aliyuncs.com/google_containers`手动拉取镜像

        - ```bash
          $ kubeadm config images pull --image-repository=registry.aliyuncs.com/google_containers
          ```

      - 用`docker tag`给拉取下来的镜像改名，`iamgeName`代指每个镜像

        - ```bash
          $ docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName k8s.gcr.io/$imageName
          ```

      - 启用`kubelet`服务

        - ```bash
          $ systemctl enable kubelet`
          ```

      - 初始化

        - ```bash
          $ kubeadm init
          ```

      - 注意初始化步骤最后打印出来的信息，按步骤执行

        - ```bash
          $ mkdir -p $HOME/.kube
          $ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
          $ sudo chown $(id -u):$(id -g) $HOME/.kube/config
          ```

      - 安装网络插件`flannel`

        - ```bash
          $ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
          
          $ sudo vim /etc/kubernetes/manifests/kube-controller-manager.yaml
          添加
          --allocate-node-cidrs=true
          --cluster-cidr=10.244.0.0/16
          ```

      - 重启`kubelet`

        - ```bash
          $ systemctl start kubelet`
          ```

      - `kubernetes`默认策略是`worker`节点运行`Pod`，`master`节点不运行`Pod`。如果需要部署单节点集群，可以通过以下的命令设置

        - ```bash
          $ kubectl taint nodes --all node-role.kubernetes.io/master-
          ```

      - 查看是否安装成功，`STATUS`需要全部是`Running`

        - ```bash
          $ kubectl get pods -n kube-system
          
          NAME                                      READY   STATUS    RESTARTS   AGE
          coredns-64897985d-4c7wf                   0/1     Running   1          2d14h
          coredns-64897985d-lf55v                   0/1     Running   1          2d14h
          etcd-vm-12-11-ubuntu                      1/1     Running   1          2d14h
          kube-apiserver-vm-12-11-ubuntu            1/1     Running   1          2d14h
          kube-controller-manager-vm-12-11-ubuntu   1/1     Running   1          2d14h
          kube-flannel-ds-cktqc                     1/1     Running   7          2d14h
          kube-proxy-r849w                          1/1     Running   1          2d14h
          kube-scheduler-vm-12-11-ubuntu            1/1     Running   1          2d14h
          ```

  - 安装`helm`

    - ```bash
      $ curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
      $ sudo apt-get install apt-transport-https --yes
      $ echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
      $ sudo apt-get update
      $ sudo apt-get install helm
      ```

      



### 预备知识

- go package, mod, import的联系和区别
- io.reader

- **`Web`基础**

  - [Go搭建一个简单的Web服务](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/03.2.md)
  - Go处理HTTP1请求和响应
  - [Go处理JSON消息](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/07.2.md)

  - Go处理GRPC请求和响应

- **`Web`原理**
  - [Go如何使得Web工作](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/03.3.md)
  - [Go的HTTP包详解](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/03.4.md)