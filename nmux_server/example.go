package nmux_server

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"google.golang.org/grpc"

	"golang.org/x/net/context"

	grpcformat "gitlab.com/tttachikoma/nmux/nmux_grpc"

	"gitlab.com/tttachikoma/nmux/nmux"

	"github.com/julienschmidt/httprouter"
)

var c chan os.Signal = make(chan os.Signal, 1)
var logger *log.Logger

func init() {
	pwd, _ := os.Getwd()
	os.Mkdir(pwd+"/log", 0777)
	logFile, err := os.OpenFile(pwd+"/log/"+time.Now().Format("20060102150405")+".log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	mw := io.MultiWriter(os.Stdout, logFile)
	logger = log.New(mw, "", log.Ldate|log.Ltime|log.Lshortfile)
	if err != nil {
		logger.Panicln(err)
	}
	logger.Println("-----------------------------------------------------")
}

type Message struct {
	Message string `json:"message"`
}

func PrintRequest(r *http.Request) {
	logger.Printf("Request Method: %v", r.Method)
	logger.Printf("Request Header: %v", r.Header)
	logger.Printf("Request URL path: %s", r.URL.Path)
	logger.Printf("Request Proto: %s", r.Proto)
}

// Error Server
type ErrorHandler struct{}

func (e *ErrorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger.Println("No matcher is macthed, so it's an error request")
	PrintRequest(r)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)

	body, err := json.Marshal(Message{Message: "No matcher is macthed!"})
	if err != nil {
		logger.Panicf("Marshal Response Body Error: %s", err.Error())
	}
	w.Write(body)
}

func serveError(l net.Listener) {
	errorServer := &http.Server{
		Handler: &ErrorHandler{},
	}
	logger.Println("Start ServeError")
	if err := errorServer.Serve(l); err == nmux.ErrServerClosed {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()
		if err := errorServer.Shutdown(ctx); err != nil {
			logger.Panicf("ServeError Shutdown Failed, Error: %s", err.Error())
		}
		logger.Println("Stop ServeError")
	} else {
		logger.Panicln(err)
	}
}

// HTTP Server
type Person struct {
	Name        string `json:"id"`
	Institution string `json:"institution"`
	Age         int    `json:"age"`
}

func Welcome(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	logger.Println("A new HTTP1 request arrives and Welcome is called")
	PrintRequest(r)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	body, err := json.Marshal(Message{Message: "Welcome to netease!"})
	if err != nil {
		logger.Panicf("Marshal Response Body Error: %s", err.Error())
	}
	w.Write(body)
}

func FormatAge(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	logger.Println("A new HTTP1 request arrives and FormatAge is called")
	PrintRequest(r)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.Panicf("Request Body Error: %s", err.Error())
	}
	person := &Person{}
	err = json.Unmarshal(body, person)
	if err != nil {
		logger.Panicf("Unmarshal Request Body Error: %s", err.Error())
	}
	logger.Printf("Request Body: %v", person)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	body, err = json.Marshal(Message{Message: "Hello, my age is " + strconv.Itoa(person.Age)})
	if err != nil {
		logger.Panicf("Marshal Response Body Error: %s", err.Error())
	}
	w.Write(body)
}

func StopMuxServer(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	logger.Println("A new HTTP1 request arrives and StopMuxServer is called")
	PrintRequest(r)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	body, err := json.Marshal(Message{Message: "The server is shutting down"})
	if err != nil {
		logger.Panicf("Marshal Response Body Error: %s", err.Error())
	}
	w.Write(body)

	c <- syscall.SIGINT
}

func serveHTTP(l net.Listener) {
	router := httprouter.New()
	router.GET("/ping", Welcome)
	router.POST("/age", FormatAge)
	router.GET("/stop", StopMuxServer)

	http1Server := &http.Server{
		Handler: router,
	}
	logger.Println("Start ServeHTTP")
	if err := http1Server.Serve(l); err == nmux.ErrServerClosed {
		ctx, cancel := context.WithTimeout(context.Background(), 8*time.Second)
		defer cancel()
		if err := http1Server.Shutdown(ctx); err != nil {
			logger.Panicf("ServeHTTP Shutdown Failed, Error: %s", err.Error())
		}
		logger.Println("Stop ServeHTTP")
	} else {
		logger.Panicln(err)
	}
}

// GRPC Server
type grpcServer struct {
	grpcformat.UnimplementedFormatServer
}

func (s *grpcServer) FormatName(ctx context.Context, in *grpcformat.NameRequest) (
	*grpcformat.FormatReply, error) {
	logger.Printf("A new gRPC request arrives and FormatName is called and the name is %s", in.Name)
	return &grpcformat.FormatReply{Message: "Hello, my name is " + in.Name}, nil
}

func (s *grpcServer) FormatInstitution(ctx context.Context, in *grpcformat.InstitutionRequest) (
	*grpcformat.FormatReply, error) {
	logger.Printf("A new gRPC request arrives and FormatName is called and the institution is %s", in.Institution)
	return &grpcformat.FormatReply{Message: "Hello, my institution is " + in.Institution}, nil
}

func (s *grpcServer) StopMuxServer(ctx context.Context, in *grpcformat.StopRequest) (
	*grpcformat.FormatReply, error) {
	logger.Printf("A new gRPC request arrives and StopMuxServer is called")
	defer func() { c <- syscall.SIGINT }()
	return &grpcformat.FormatReply{Message: "The server is shutting down"}, nil
}

func serveGRPC(l net.Listener) {
	grpcs := grpc.NewServer()
	grpcformat.RegisterFormatServer(grpcs, &grpcServer{})

	logger.Println("Start ServeGRPC")
	if err := grpcs.Serve(l); err == nmux.ErrServerClosed {
		grpcs.GracefulStop()
		logger.Println("Stop ServeGRPC")
	} else {
		logger.Panicln(err)
	}
}

func Example(ip string) {
	signal.Notify(c, []os.Signal{syscall.SIGTERM, syscall.SIGINT}...)
	logger.Printf("Listen on %s", ip)
	l, err := net.Listen("tcp", ip)
	if err != nil {
		logger.Panicln(err)
	}
	m := nmux.New(l)

	grpcMacther := nmux.NewGRPCMatcher("content-type", "application/grpc")
	http1Macther := nmux.NewHTTP1Macther("Content-Type", "application/json")
	errorMatcher := nmux.Any()

	// We first match the connection against HTTP2 fields. If matched, the
	// connection will be sent through the "grpcListener" listener.
	grpcListener := m.RegisterMatcher(grpcMacther)
	// Otherwise, we match it againts HTTP1 fields. If matched,
	// it is sent through the "http1Listener" listener.
	http1Listener := m.RegisterMatcher(http1Macther)
	// If not matched by above matchers, we assume it is an error connection.
	errorListener := m.RegisterMatcher(errorMatcher)
	// Then we used the muxed listeners.
	go serveGRPC(grpcListener)
	go serveHTTP(http1Listener)
	go serveError(errorListener)

	go func() {
		<-c
		logger.Println("An abort signal arrives and NMUX server will to be closed gracefully")
		m.Close()
	}()

	logger.Println("Start NMUX Server")
	if err := m.Serve(); !strings.Contains(err.Error(), "use of closed network connection") {
		logger.Panicln(err)
	}
	time.Sleep(10 * time.Second)
	logger.Println("Stop NMUX Server")
}
