module gitlab.com/tttachikoma/nmux

go 1.18

require (
	github.com/julienschmidt/httprouter v1.3.0
	golang.org/x/net v0.0.0-20220403103023-749bd193bc2b
	google.golang.org/grpc v1.45.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20200806141610-86f49bd18e98 // indirect
	google.golang.org/protobuf v1.27.1
)
